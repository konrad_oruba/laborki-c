/*
 * =====================================================================================
 *
 *       Filename:  z3.cc
 *
 *
 *        Created:  24.10.2014 18:55:38
 *
 *         Author:  Konrad Oruba 
 *         Email:   konradxxx3@gmail.com
 *
 * =====================================================================================
 */
 
#include<iostream>

using namespace std;

class Licznik{
public:
		Licznik();
		void zwieksz();
		void zmniejsz();
		int getWartosc();

private:
	int wartosc=0;
};


int main()
{
		system("clear");
		Licznik l;
		l.zwieksz();
		l.zwieksz();
		l.zwieksz();
		l.zwieksz();

		cout<<"teraz jest: "<<l.getWartosc()<<endl;
		l.zmniejsz();
		cout<<"teraz jest: "<<l.getWartosc()<<endl;
		
return 0;
}
		Licznik::Licznik(){}
		void Licznik::zwieksz(){
				wartosc++;
		}
		void Licznik::zmniejsz(){
				wartosc--;
		}
		int Licznik::getWartosc(){
				return wartosc;
		}

