#include <iostream>
#include "Wymierna.h"

using namespace std;

int main()
{
    system("clear");

    Wymierna w(12,-24);
    Wymierna ww(10);
    cout<<"licznik w: "<<w.getLicznik()<<endl;
    cout<<"mianownik w: "<<w.getMianownik()<<endl;
    cout<<"double w: "<<w.to_double()<<endl;
    Wymierna pw=-w;
    cout<<"przeciwna w: "<<pw.to_double()<<endl<<endl;

    cout<<"licznik ww: "<<ww.getLicznik()<<endl;
    cout<<"mianownik ww: "<<ww.getMianownik()<<endl;
    cout<<"double ww: "<<ww.to_double()<<endl;
    Wymierna pww=-ww;
    cout<<"przeciwna ww: "<<pww.to_double()<<endl<<endl;

    cout<<"w<ww ";
    if (w<ww)
        cout<<"jest mniejsza"<<endl;
    else
        cout<<"nie jest mniejsza"<<endl;

    cout<<"w==ww ";
    if (w==ww)
        cout<<"jest rowna"<<endl;
    else
        cout<<"nie jest rowna"<<endl;
    cout<<endl;
    cout<<"w+ww="<<w+ww<<endl;
    cout<<"w*ww="<<w*ww<<endl;

    return 0;
}
