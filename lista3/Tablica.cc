#include <iostream>
#include "Tablica.h"

using namespace std;

Tablica::Tablica(int lengthTMP, long pocz)
{
    length=lengthTMP;
    tab= new long[length] ;

    for(int i=0; i<length; i++) {
        tab[i]=pocz;
    }
}

Tablica::~Tablica() {
    cout<<"dziala czyszciciel"<<endl;
    delete [] tab;
}

Tablica::Tablica(const Tablica &other) {

    cout<<"dziala kopiujacy "<<endl;
    length=other.length;
    tab=new long[other.length];
    for(int i=0; i<other.length; i++) {
        tab[i]=other.tab[i];

    }
}

Tablica& Tablica::operator =(const Tablica& t) {
    cout<<"przypisywacz"<<endl;

    if (this == &t) {
        return *this;
    }
    length=t.length;
    delete [] tab;
    tab = nullptr;
    tab=new long[length];

    for(int i=0; i<t.length; i++) {
        tab[i]=t.tab[i];

    }
    return *this;
}



int Tablica::getLength()const {
    return length;
}

void Tablica::setElement(int ind, long wart) {
    tab[ind]=wart;
}

long Tablica::getElement(int ind)const {
    return tab[ind];
}


