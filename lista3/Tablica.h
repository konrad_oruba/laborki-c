#ifndef TABLICA_H
#define TABLICA_H


class Tablica
{
public:
    Tablica(int lengthTMP, long pocz=0);
    Tablica(const Tablica&);
    Tablica& operator=(const Tablica&);
    ~Tablica();
    int getLength()const;
    void setElement(int,long);
    long getElement(int)const;
private:
    int length;
    long* tab;
};


#endif
