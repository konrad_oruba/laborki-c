#include <iostream>
#include "Tablica.h"

using namespace std;

void wypisz(const Tablica*);
int main()
{
    Tablica a(12);
    cout<<"a: ";
    wypisz(&a);
    Tablica b(a);
    cout<<"b: ";
    wypisz(&b);

    for(int j=0; j<a.getLength(); j++) {
        a.setElement(j,100-j);
    };

    cout<<"a: ";
    wypisz(&a);
    b=a;
    cout<<"b: ";
    wypisz(&b);

    return 0;
}

void wypisz(const Tablica *t) {
    cout<<"wypisz: ";
    int rozm=t->getLength();
    for (int i=0; i<rozm; i++) {
        cout<<t->getElement(i)<<" ";
    }
    cout<<endl;
}
