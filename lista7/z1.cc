#include <sstream>
#include <iostream>

using namespace std;

class Licznik{
		friend ostream& operator<<(ostream&, Licznik const&); 
public:
		Licznik();
		int getWartosc();
		Licznik& operator++();
		Licznik& operator--();
		Licznik operator++(int);
		Licznik operator--(int);
		operator int() const;	

private:
	int wartosc=0;
};


int main()
{
		system("clear");
		Licznik l;
		++l;
		++l;
		cout<<"teraz jest: "<<int(l)<<endl;
		--l;
		cout<<"teraz jest: "<<int(l)<<endl;
		
		l++;
		l++;
		cout<<"teraz jest: "<<l<<endl;
		l--;
		cout<<"teraz jest: "<<l<<endl;
		cout<<endl;
		Licznik *ll=new Licznik(l);
		++*ll;
		(*ll)++;
		cout<<"teraz jest: "<<*ll<<endl;
		--*ll;
		cout<<"teraz jest: "<<*ll<<endl;
		
return 0;
}
	Licznik::Licznik(){}

		int Licznik::getWartosc(){
				return wartosc;
		}

	Licznik& Licznik::operator++(){
		wartosc=wartosc + 1;
		return *this;
	}
	
	Licznik& Licznik::operator--(){
		wartosc=wartosc - 1;
		return *this;
	}
	
	Licznik Licznik::operator++(int){
		Licznik tmp=*this;
		wartosc=wartosc+1;
		return tmp;
	}

	Licznik Licznik::operator--(int){
		Licznik tmp=*this;
		wartosc=wartosc-1;
		return tmp;
	}
	
	Licznik::operator int() const{
		return wartosc;
	}
	
	ostream& operator<<(ostream& out, Licznik const& dc)
	{
		ostringstream oss;
		oss << dc.wartosc; 
		out << oss.str();
		return out;
	}
