#ifndef OSOBA_H

#include <string>

using namespace std;

class Osoba
{
public:
    // konstruktory przeciążone
    Osoba(const string& imie, const string& nazwisko, int rokUrodzenia );
    Osoba(const string& imie, const string& nazwisko);
    Osoba(const Osoba&);
    ~Osoba();
    void przedstawSie() const;
    static int getIle();
private:
    static int ile;
private:
    string imie;
    string nazwisko;
    int rokUrodzenia;
};

#else
#endif
