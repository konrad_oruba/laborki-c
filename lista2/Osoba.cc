#include <iostream>
#include "Osoba.h"

using namespace std;

int Osoba::ile = 0;

Osoba::Osoba(const string& imie, const string& nazwisko, int rokUrodzenia)
    : imie(imie),nazwisko(nazwisko), rokUrodzenia(rokUrodzenia)
{
    ++ile;
}

Osoba::Osoba(const string& imie, const string& nazwisko)
    : imie(imie), nazwisko(nazwisko), rokUrodzenia(1)
{
    ++ile;
}

Osoba::Osoba(const Osoba& other)
{
    cerr << "Konstruktor kopiujący" << endl;
    imie = other.imie;
    nazwisko = other.nazwisko;
    rokUrodzenia = other.rokUrodzenia;
    ++ile;
}

Osoba::~Osoba()
{
    --ile;
}

void Osoba::przedstawSie() const
{
    cout<<"mam na imie: "<<imie<<" nazywam sie: "<<nazwisko<<" urodzony w: " <<rokUrodzenia<<" roku"<<endl;
}

int Osoba::getIle()
{
    return ile;
}
